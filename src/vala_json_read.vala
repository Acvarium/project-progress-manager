
public errordomain MyError {
	INVALID_FORMAT
}

public static ProjectInfo process_project (Json.Node node, uint number) throws Error {
	if (node.get_node_type () != Json.NodeType.OBJECT) {
		throw new MyError.INVALID_FORMAT ("Unexpected element type %s", node.type_name ());
	}

	unowned  Json.Object obj = node.get_object ();

	ProjectInfo info = new ProjectInfo();
	info.name = obj.get_string_member ("name");
	if (obj.has_member("description"))
		info.description = obj.get_string_member ("description");
	if (obj.has_member("icon"))
		info.icon = obj.get_string_member ("icon");
	if (obj.has_member("marks")){
		Json.Array marksArray = obj.get_array_member("marks");

		foreach (unowned Json.Node markNode in marksArray.get_elements ()) {
			unowned  Json.Object mark = markNode.get_object ();
			Mark m = new Mark();
			if (mark.has_member("date"))
				m.dateTime = mark.get_string_member("date");
			if (mark.has_member("comment"))
				m.comment = mark.get_string_member("comment");
			info.marks.append(m);
		}
	}
	return info;
}

public static void PrintProjectInfo(ProjectInfo info){

	print("------------------------------\n");
	print(info.name + "\n");
	print("   descr:  " + info.description + "\n");
	print("   icon :  " + info.icon + "\n");

	foreach(Mark m in info.marks){
		print("#");
	}
	print("\n");

	foreach(Mark m in info.marks){
		print(m.dateTime + "  " + m.comment + "\n");
	}
	print("\n\n");
}

public static int main (string[] args) {
	var file = File.new_for_path("projects.json");
	List<ProjectInfo> projectsInfo = new List<ProjectInfo>();

	if (!file.query_exists()){
		stderr.printf("File '%s' doesn't exist.\n", file.get_path());
		return 1;
	}
	string data = "";
	try {
		var dis = new DataInputStream (file.read());
		string line;
		while ((line = dis.read_line (null)) != null) {
			data += line;
		}
	} catch (Error e) {
		error ("%s", e.message);
	}

	// Parse:
	Json.Parser parser = new Json.Parser ();
	try {
		parser.load_from_data (data);

		// Get the root node:
		Json.Node node = parser.get_root ();

		if (node.get_node_type () != Json.NodeType.ARRAY) {
			throw new MyError.INVALID_FORMAT ("Unexpected element type %s", node.type_name ());
		}
		unowned Json.Array array = node.get_array ();
		int i = 1;
	
		foreach (unowned Json.Node item in array.get_elements ()) {
			projectsInfo.append(process_project(item, i));
			i++;
		}

		foreach(ProjectInfo info in projectsInfo){
			PrintProjectInfo(info);
		}

		
		SaveData("~projects.json", InfoTostring(projectsInfo));
		//  process (node);
	} catch (Error e) {
		print ("Unable to parse the string: %s\n", e.message);
		return -1;
	}
	return 0;
}

public class Mark : Object {
	public string dateTime;
	public string comment;
}

public class ProjectInfo{
	public string name = "";
	public string description = "";
	public string icon = "";
	public List<Mark> marks = new List<Mark>();
}


public string InfoTostring(List<ProjectInfo> projectsInfo){
	Json.Builder builder = new Json.Builder ();
	builder.begin_array ();
	foreach(ProjectInfo info in projectsInfo){
		builder.begin_object ();
		builder.set_member_name ("name");
		builder.add_string_value (info.name);
		builder.set_member_name ("description");
		builder.add_string_value (info.description);
		builder.set_member_name ("icon");
		builder.add_string_value (info.icon);
		if (info.marks.length() > 0){
			builder.set_member_name ("marks");
			builder.begin_array ();
			foreach(Mark mark in info.marks){
				builder.begin_object ();
				builder.set_member_name ("date");
				builder.add_string_value (mark.dateTime);
				if (mark.comment != null){
					builder.set_member_name ("comment");
					builder.add_string_value (mark.comment);
				}
				builder.end_object ();
			}
			builder.end_array ();
		}
		builder.end_object ();

	}
	builder.end_array ();

//------------------------------------------------------
	Json.Generator generator = new Json.Generator ();
	generator.pretty = true;
    
	Json.Node root = builder.get_root ();
	generator.set_root (root);

	string str = generator.to_data (null);
	return str;
}

public void SaveData(string fileName, string data){
	var file = File.new_for_path (fileName);
	if (file.query_exists ()) {
		file.delete ();
	}

	// Create a new file with this name
	var file_stream = file.create (FileCreateFlags.NONE);

	// Test for the existence of file
	if (file.query_exists ()) {
		stdout.printf ("Data successfully saved.\n");
	}

	// Write text data to file
	var data_stream = new DataOutputStream (file_stream);
	data_stream.put_string (data);
}

//vala --pkg json-glib-1.0 --pkg gio-2.0 vala_json_read.vala 